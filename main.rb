require_relative 'time_tracker'

filename = 'time_tracking.csv'
time_tracker = TimeTracker.new(filename)

loop do
  puts ""
  puts "Options:"
  puts "1. List tasks"
  puts "2. Track time"
  puts "3. Exit"

  print "Select an option: "
  choice = gets.chomp.to_i

  case choice
  when 1
    time_tracker.list_tasks
  when 2
    print "Enter project name: "
    project = gets.chomp
    print "Enter task description: "
    task = gets.chomp
    print "Enter hours spent: "
    hours = gets.chomp.to_f

    time_tracker.track_time(project, task, hours)
  when 3
    puts ""
    puts "Exiting Time Tracker. Goodbye!"
    break
  else
    puts ""
    puts "Invalid choice. Please select a valid option."
  end
end