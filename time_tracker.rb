require 'csv'
require 'time'

class TimeTracker
  def initialize(csv_file)
    @csv_file = csv_file
    @tasks = []

    load_data
  end

  def load_data
    if File.exist?(@csv_file)
      @tasks = CSV.read(@csv_file, headers: true, header_converters: :symbol)
    end
  end

  def save_data
    CSV.open(@csv_file, 'w', write_headers: true, headers: @tasks.first.keys) do |csv|
      @tasks.each { |task| csv << task.values }
    end
  end

  def list_tasks
    puts ""
    puts "Time Tracking Summary:"
    @tasks.each do |task|
      puts "Project: #{task[:project]} | Task: #{task[:task]} | Hours: #{task[:hours]} | Date: #{task[:date]}"
    end
  end

  def track_time(project, task, hours)
    date = Time.now.strftime('%Y-%m-%d')
    @tasks << { project: project, task: task, hours: hours, date: date }
    save_data
    puts "Time tracked for #{project}: #{hours} hours on #{date}"
  end
end